import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';

export  function showFighterDetailsModal(fighter) {
  const title = 'Fighter info';
  const bodyElement = createFighterDetails(fighter);
  showModal({ title, bodyElement });
}

export function createFighterDetails(fighter) {
  const { name, attack, defense, health, source } = fighter;

  const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });
  const fighterName = createSpanElement(`Name: ${name}\n`);
  const fighterAttack = createSpanElement(`Attack: ${attack}\n`);
  const fighterDefense = createSpanElement(`Defense: ${defense}\n`);
  const fighterHealth = createSpanElement(`Health: ${Math.round(health)}\n`);
  const fighterImage = createImageElement(source);
  fighterDetails.append(fighterName, fighterAttack, fighterDefense, fighterHealth, fighterImage);

  return fighterDetails;
}

function createSpanElement(text) {
  const element = createElement({ tagName: 'span' });
  element.innerText = text;
  return element;
}

function createImageElement(source) {
  const attributes = { src: source };
  const imgElement = createElement({ tagName: 'img', className: 'fighter-image', attributes });
  return imgElement;
}
