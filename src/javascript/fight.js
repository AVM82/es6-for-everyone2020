
var firstTurn = true;

export function fight(firstFighter, secondFighter) {
  do {
    firstTurn ? secondFighter.health -= getDamage(firstFighter, secondFighter) : firstFighter.health -= getDamage(secondFighter, firstFighter);
    firstTurn = !firstTurn;
  } while (firstFighter.health > 0 && secondFighter.health > 0);
  return (firstFighter.health > 0) ? firstFighter : secondFighter;
}

export function getDamage(attacker, enemy) {
  var damage = getHitPower(attacker) - getBlockPower(enemy);
  return damage < 0 ? 0 : damage;
}

export function getHitPower(fighter) {
  const criticalHitChance = random(1, 2);
  return fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter) {
  const dodgeChance = random(1, 2);
  return  fighter.defense * dodgeChance;
}

function random(min, max) {
  return min + (max - min) * Math.random();
}
